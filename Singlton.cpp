#include "Singlton.hpp"

#include <iostream>

Singlton::Singlton(void) {
}

void Singlton::hello(void) {
	std::cout << "Hello, singltone!" << std::endl;
}

Singlton &Singlton::getInstance(void) {
	static Singlton s;
	return s;
}
