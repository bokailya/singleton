LINK.o = $(LINK.cc)
CXX = g++
CXXFLAGS=-Wall -Wextra --std=c++0x

program: program.o Singlton.o

program.o: program.cpp
Singlton.o: Singlton.cpp Singlton.hpp
