#ifndef SINGLTON_HPP
#define SINGLTON_HPP

class Singlton {
	public:
		void hello(void);
		static Singlton &getInstance(void);

		Singlton(const Singlton &s) = delete;
		void operator=(const Singlton &s) = delete;
	private:
		Singlton(void);
};

#endif // SINGLTON_HPP
